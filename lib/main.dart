import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:google_fonts/google_fonts.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkVisibilityKeyboard();

  }

  int done = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: (){
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: Stack(
          children: [
            AnimatedPositioned(
              duration: const Duration(milliseconds: 250),
              right: -40,
              left: -40,
              top: visibilityKeyboard ? -MediaQuery.of(context).size.width : (-MediaQuery.of(context).size.width / 3),
              child: Container(
                width: MediaQuery.of(context).size.width * 2,
                height: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width),
                    color: Colors.deepOrange
                ),
              ),
            ),
            AnimatedPositioned(
              left: (MediaQuery.of(context).size.width - logoSize) / 2,
              top: visibilityKeyboard ? 80 : 170,
              duration: const Duration(milliseconds: 250),
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 250),
                width: logoSize.toDouble(),
                height: logoSize.toDouble(),
                padding: EdgeInsets.all(visibilityKeyboard ? 10 : 0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: visibilityKeyboard ? const Color(0xff0d2237) : Colors.transparent
                ),
                child: ClipRRect(borderRadius: BorderRadius.circular(30),child: Image.asset('img/mj.jpeg',width: logoSize.toDouble(),height: logoSize.toDouble(),)),
              ),
            ),
            AnimatedPositioned(
              top: visibilityKeyboard ? ((MediaQuery.of(context).size.width / 1.5) + 70)-150 : (MediaQuery.of(context).size.width / 1.5) + 50,
              width: MediaQuery.of(context).size.width,
              duration: const Duration(milliseconds: 250),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("سجل الدخول الى حسابك الان",textAlign: TextAlign.center,
                      style: GoogleFonts.notoKufiArabic(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.black),),
                      const SizedBox(height: 20,),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        textAlign: TextAlign.right,
                        controller: phone,
                        onChanged: (v){
                          setState(() {
                          });
                        },
                        style: GoogleFonts.notoKufiArabic(fontSize: 11,color: Colors.black),
                        decoration: InputDecoration(
                          hintText: 'رقم الهاتف',
                          hintStyle: GoogleFonts.notoKufiArabic(fontSize: 11,color: Colors.black),
                          suffixIcon: const Icon(Icons.phone,color: Colors.black38,),
                          counterStyle: const TextStyle(color: Colors.black54),
                          labelStyle: const TextStyle(color: Colors.black54),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:  const BorderSide(color: Colors.black12),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:  const BorderSide(color: Colors.black12),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:  const BorderSide(color: Colors.black12),
                          ),
                          fillColor: Colors.white,
                          filled: true,
                        ),
                      ),
                      const SizedBox(height: 10,),
                      TextFormField(
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                        textAlign: TextAlign.right,
                        controller: password,
                        onChanged: (v){
                          setState(() {
                          });
                        },
                        style: GoogleFonts.notoKufiArabic(fontSize: 11,color: Colors.black),
                        decoration: InputDecoration(
                          hintText: 'كلمة المرور',
                          hintStyle: GoogleFonts.notoKufiArabic(fontSize: 11,color: Colors.black),
                          suffixIcon: const Icon(Icons.password,color: Colors.black38,),
                          counterStyle: const TextStyle(color: Colors.black54),
                          labelStyle: const TextStyle(color: Colors.black54),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:  const BorderSide(color: Colors.black12),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:  const BorderSide(color: Colors.black12),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:  const BorderSide(color: Colors.black12),
                          ),
                          fillColor: Colors.white,
                          filled: true,
                        ),
                      ),
                      const SizedBox(height: 7,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: GestureDetector(
                              onTap: (){

                              },
                              child: Text("ليس لديك اي حساب ؟ هل تريد انشاء حساب",textAlign: TextAlign.center,
                                style: GoogleFonts.notoKufiArabic(fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black38),),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 15,),


                    ],
                  ),
                ),
              ),
            ),
            AnimatedPositioned(
              top: done == 1 ? 0 : -MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              duration: const Duration(milliseconds: 0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Colors.black38,
              ),
            ),
            AnimatedPositioned(
              top: done == 2 ? -100 : (done == 1 ? ((MediaQuery.of(context).size.height / 2) - 40) : (visibilityKeyboard ? ((MediaQuery.of(context).size.width / 1.5) + 140) : (MediaQuery.of(context).size.width / 1.5) + 270)),
              left: done == 2 ? -100 : (done == 1 ? ((MediaQuery.of(context).size.width / 2) - 40) : 20),
              right: done == 2 ? -100 : (done == 1 ? ((MediaQuery.of(context).size.width / 2) - 40) : 20),
              bottom: done == 2 ? -100 : (done == 1 ? ((MediaQuery.of(context).size.height / 2) - 40) : null),
              duration: const Duration(milliseconds: 250),
              child: GestureDetector(
                onTap: () async {
                  setState(() {
                    done = 1;
                  });
                  Future.delayed(const Duration(milliseconds: 1500), () {
                    setState(() {
                      done = 2;
                    });
                  });
                },
                child: AnimatedContainer(
                  duration: const Duration(milliseconds: 250),
                  width: done == 2 ? MediaQuery.of(context).size.width : (done == 1 ? 50 : MediaQuery.of(context).size.width),
                  height: done == 2 ? MediaQuery.of(context).size.height : 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(done == 0 ? 10 : 1000),
                    color: done == 2 ? Colors.white : Colors.deepOrange,
                  ),
                  child: Center(
                    child: done == 1 ? const CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color> (Colors.white)) : Text(done == 0 ? "تسجيل" : "Home Page",textAlign: TextAlign.center,
                      style: GoogleFonts.notoKufiArabic(fontSize: 13,fontWeight: FontWeight.bold,color: done == 0 ? Colors.white : Colors.black),),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }



  TextEditingController phone = TextEditingController();
  TextEditingController password = TextEditingController();
  bool visibilityKeyboard = false;
  int logoSize = 150;
  checkVisibilityKeyboard(){
    KeyboardVisibilityController _keyboardVisibilityController = KeyboardVisibilityController();
    _keyboardVisibilityController.onChange.listen((bool visible) {
      if(mounted){
        if(visible){
          logoSize = 100;
        }else{
          logoSize = 150;
        }
        setState(() {
          visibilityKeyboard = visible;
        });
      }
    });
  }
}

